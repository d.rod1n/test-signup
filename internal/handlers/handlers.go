package handlers

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"test-signup/internal/store/errstorage"
)

type StorageUsers interface {
	Registration(ctx context.Context, login string, password string) (int, error)
	Authentication(ctx context.Context, login string, password string) (int, error)
}

type UserJSON struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type ErrResponse struct {
	Answer string `json:"error"`
}

type Response struct {
	UserID int `json:"user_id"`
}

type Handlers struct{}

func (h *Handlers) SignUp(res http.ResponseWriter, req *http.Request, st StorageUsers) {
	var buf bytes.Buffer
	_, err := buf.ReadFrom(req.Body)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	var userJSON UserJSON
	if err := json.Unmarshal(buf.Bytes(), &userJSON); err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	ctx := req.Context()
	userID, err := st.Registration(ctx, userJSON.Login, userJSON.Password)
	if err != nil {

		if errors.Is(errstorage.ErrUserExist, err) {
			resp := ErrResponse{Answer: "user exist"}
			ans, err := json.Marshal(resp)
			if err != nil {
				res.WriteHeader(http.StatusInternalServerError)
				return
			}

			res.WriteHeader(http.StatusConflict)
			res.Write(ans)
			return
		}

		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp := Response{UserID: userID}
	ans, err := json.Marshal(resp)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write(ans)
}

func (h *Handlers) SignIn(res http.ResponseWriter, req *http.Request, st StorageUsers) {
	var buf bytes.Buffer
	_, err := buf.ReadFrom(req.Body)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	var userJSON UserJSON
	if err := json.Unmarshal(buf.Bytes(), &userJSON); err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	ctx := req.Context()
	userID, err := st.Authentication(ctx, userJSON.Login, userJSON.Password)
	if err != nil {
		if errors.Is(errstorage.ErrLoginPassword, err) {
			resp := ErrResponse{Answer: "login/password errors"}
			ans, err := json.Marshal(resp)
			if err != nil {
				res.WriteHeader(http.StatusInternalServerError)
				return
			}

			res.WriteHeader(http.StatusConflict)
			res.Write(ans)
			return
		}
	}

	resp := Response{UserID: userID}
	ans, err := json.Marshal(resp)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write(ans)
}

type StoragePing interface {
	Ping(ctx context.Context) error
}

type PingResponse struct {
	Answer string `json:"answer"`
}

func (h *Handlers) Ping(res http.ResponseWriter, req *http.Request, st StoragePing) {
	ctx := req.Context()
	if err := st.Ping(ctx); err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	a := &PingResponse{
		Answer: "db is connect",
	}
	ans, err := json.Marshal(a)
	if err != nil {
		log.Printf("failed to marshall answer: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	res.Write(ans)

}

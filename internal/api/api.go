package api

import (
	"context"
	"net/http"
	"test-signup/internal/handlers"

	"github.com/go-chi/chi/v5"
)

type API struct {
	srv *http.Server
}

func (a *API) Run() error {
	return a.srv.ListenAndServe()
}

type Storage interface {
	Registration(ctx context.Context, login string, password string) (int, error)
	Authentication(ctx context.Context, login string, password string) (int, error)
	Ping(ctx context.Context) error
}

const (
	routePing   = "/ping"
	routeSignIn = "/sign_in"
	routeSignUp = "/sign_up"
)

func NewAPI(db Storage, addr string) *API {
	h := &handlers.Handlers{}
	r := chi.NewRouter()
	r.Get(routePing, func(w http.ResponseWriter, r *http.Request) {
		h.Ping(w, r, db)
	})

	r.Post(routeSignIn, func(w http.ResponseWriter, r *http.Request) {
		h.SignIn(w, r, db)
	})

	r.Post(routeSignUp, func(w http.ResponseWriter, r *http.Request) {
		h.SignUp(w, r, db)
	})

	return &API{
		srv: &http.Server{
			Addr:    addr,
			Handler: r,
		},
	}
}

package errstorage

import "errors"

var (
	ErrUserExist     = errors.New("user exist")
	ErrLoginPassword = errors.New("login/password error")
)

package store

import (
	"context"
	"fmt"
	"log"
	"test-signup/internal/store/errstorage"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage struct {
	base *pgxpool.Pool
}

func (st *Storage) Ping(ctx context.Context) error {
	return st.base.Ping(ctx)
}

func (st *Storage) Registration(ctx context.Context, login string, password string) (int, error) {
	row := st.base.QueryRow(ctx, `
	with newUser as (
		select 
		$1 as login,
		$2 as password
	),
	dupData as (
		select 
			user_id
		from users
		where login in (select login from newUser)
	),

	insData as (
		insert into users (login, password)
		select * from newUser
		where (login) not in (select login from dupData)
		returning user_id
	---	
	)
	select user_id, true as result from dupData union all select user_id, false as result from insData;`, login, password)
	var userID int
	var status bool

	if err := row.Scan(&userID, &status); err != nil {
		log.Println("failed to scan query")
		return 0, fmt.Errorf("failed to scan query: %w", err)
	}
	log.Println("user_id", userID)
	if status {
		return 0, errstorage.ErrUserExist
	}
	return userID, nil
}

func (st *Storage) Authentication(ctx context.Context, login string, password string) (int, error) {
	row := st.base.QueryRow(ctx, "select user_id from users where login=$1 and password=$2;", login, password)
	var userID int
	err := row.Scan(&userID)
	if err != nil {
		log.Println("failed to scan query")
		return 0, fmt.Errorf("failed to scan query: %w", err)
	}

	if userID == 0 {
		return 0, errstorage.ErrLoginPassword
	}
	return userID, nil
}

func createDB(ctx context.Context, db *pgxpool.Pool) error {
	if _, err := db.Exec(ctx, `create table if not exists users(
		user_id SERIAL PRIMARY KEY, 
		login VARCHAR(50) unique,
		password VARCHAR(50));

		insert into users(login, password) values('admin', 'admin') on conflict do nothing;
		`); err != nil {
		return fmt.Errorf("failed to create urls table: %v", err)
	}
	return nil
}

func NewStorage(dsn string) (*Storage, error) {
	ctx := context.Background()
	base, err := pgxpool.New(ctx, dsn)
	if err != nil {
		return nil, fmt.Errorf("failed to open database: %v", err)
	}

	if err := createDB(ctx, base); err != nil {
		return nil, fmt.Errorf("failed to create database: %v", err)
	}
	return &Storage{
		base: base,
	}, nil
}

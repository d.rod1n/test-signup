package config

import (
	"fmt"
	"os"
)

type Config struct {
	DatabaseDSN string
	Port        string
	Addr        string
}

func NewConfig() *Config {
	nameDB := os.Getenv("DB_NAME")
	userName := os.Getenv("DB_USERNAME")
	pwd := os.Getenv("DB_PASSWORD")
	dataSourceName := ""
	if nameDB != "" && userName != "" && pwd != "" {
		dataSourceName = fmt.Sprintf("host=db user=%s password=%s dbname=%s sslmode=disable", userName, pwd, nameDB)
	}

	addr := os.Getenv("ADDR")
	cfg := &Config{
		DatabaseDSN: dataSourceName,
		Addr:        addr,
	}
	return cfg

}

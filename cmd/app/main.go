package main

import (
	"fmt"
	"log"
	"test-signup/internal/api"
	"test-signup/internal/config"
	"test-signup/internal/store"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	cfg := config.NewConfig()

	db, err := store.NewStorage(cfg.DatabaseDSN)
	if err != nil {
		return fmt.Errorf("failed to init storage: %w", err)
	}

	api := api.NewAPI(db, cfg.Addr)
	log.Printf("Starting server: %v", cfg.Addr)
	if err := api.Run(); err != nil {
		return fmt.Errorf("failed to start server: %w", err)
	}

	return nil
}
